# frozen_string_literal: true

require "rails_helper"
RSpec.describe TranslatorWorker, type: :worker do
  describe "#perform" do
    it "creates a new translation" do
      user = create(:user)
      translation = create(:translation, user: user)
      fake_translator_service = double(TranslatorService)
      allow(fake_translator_service).to receive(:translate).and_return("some translation")
      allow(TranslatorService).to receive(:new).and_return(
        fake_translator_service
      )

      described_class.new.perform(
        translation_id: translation.id,
        user_id: user.id,
        target_language: "de"
      )

      expect(Translation.count).to eq(2)
    end

    it "creates a translation with the propper status" do
      user = create(:user)
      translation = create(:translation, user: user)
      fake_translator_service = double(TranslatorService)
      allow(fake_translator_service).to receive(:translate).and_return("some translation")
      allow(TranslatorService).to receive(:new).and_return(
        fake_translator_service
      )

      described_class.new.perform(
        translation_id: translation.id,
        user_id: user.id,
        target_language: "de"
      )

      expect(Translation.last.status).to eq("draft")
    end
  end
end
