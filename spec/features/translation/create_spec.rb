# frozen_string_literal: true

require "rails_helper"

RSpec.feature "CreateTranslation", type: :feature do
  scenario "manuell translation" do
    user = create(:user, name: "James")
    article = create(:article)

    visit new_article_translation_path(article)
    translation_with(article, "the answer of everything", "42", "James", "English")

    expect(page).to have_content("Translation was successfully created.")
    expect(TranslatorWorker.jobs.size).to eq 0
  end

  scenario "automatic translation" do
    article = create(:article)
    user = create(:user, name: "James")

    visit new_article_translation_path(article)
    check("Automatic translation")
    select(user.name, from: "translation[user_id]")
    select("English", from: "translation[language]")
    click_button "Create Translation"

    expect(page).to have_content("Automatic translation will be available for review shortly.")
    expect(TranslatorWorker.jobs.size).to eq 1
  end

  def translation_with(article, question, answer, user_name, language)
    fill_in "Question", with: question
    fill_in "Answer", with: answer
    select(user_name, from: "translation[user_id]")
    select(language, from: "translation[language]")
    click_button "Create Translation"
  end
end
