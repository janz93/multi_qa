# frozen_string_literal: true

require "rails_helper"

RSpec.describe "translations/show", type: :view do
  it "renders the question" do
    translation = build_stubbed(
      :translation,
      question: "What is the anwser if everything"
    )
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_content("What is the anwser if everything")
  end

  it "renders the answer" do
    translation = build_stubbed(
      :translation,
      answer: "42"
    )
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_content("42")
  end

  it "renders the language" do
    translation = build_stubbed(
      :translation,
      language: "de"
    )
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_content("🇩🇪")
  end

  it "renders the status" do
    translation = build_stubbed(
      :translation,
      status: 1
    )
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_content("published")
  end

  it "renders link back to article" do
    translation = build_stubbed(:translation)
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_link("Back", href: article_path(article))
  end

  it "renders link to edit translation" do
    translation = build_stubbed(:translation)
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_link("Edit", href: edit_article_translation_path(article, translation))
  end

  it "renders link to destroy translation" do
    translation = build_stubbed(:translation)
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_link("Destroy", href: article_translation_path(article, translation))
  end
end
