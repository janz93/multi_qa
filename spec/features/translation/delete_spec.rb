# frozen_string_literal: true

require "rails_helper"

RSpec.feature "DeleteTranslation", type: :feature do
  scenario "delete translation" do
    article = create(:article)
    translation = create_list(:translation, 2, article: article).last

    visit article_translation_path(translation.article, translation)
    click_link("Destroy", href: article_translation_path(translation.article, translation))

    expect(page).to have_content("Translation was successfully destroyed.")
    expect(current_path).to eq article_path(article)
  end

  scenario "delete translation and implicit article" do
    translation = create(:translation)

    visit article_translation_path(translation.article, translation)
    click_link("Destroy")

    expect(page).to have_content("Translation and Article were successfully destroyed.")
    expect(current_path).to eq articles_path
  end
end
