# frozen_string_literal: true

require "rails_helper"

RSpec.feature "ShowTranslation", type: :feature do
  scenario "view translation content" do
    article = create(:article)
    translation = create(
      :translation,
      answer: "My answer",
      language: "en",
      article: article
    )

    visit article_path(article)
    click_link("🇺🇸")

    expect(page).to have_content("My answer")
  end
end
