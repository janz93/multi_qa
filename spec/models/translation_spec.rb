# frozen_string_literal: true

require "rails_helper"

RSpec.describe Translation, type: :model do
  context "validate associations" do
    it { should belong_to(:user) }
    it { should belong_to(:article) }
  end

  context "validations" do
    it { should validate_presence_of(:question) }
    it { should validate_presence_of(:answer) }
    it { should validate_presence_of(:language) }

    context "orphans removal" do
      it "has no empty article records" do
        james = create(:user, name: "James")
        james_article = create(:article)
        lisa = create(:user, name: "Lisa")
        other_article = create(:article)

        create_list(:translation, 3, user: james, article: james_article)
        create_list(:translation, 2, user: james, article: other_article)
        create(:translation, user: lisa, article: other_article)

        expect { james.destroy }.to change { Article.count }.from(2).to(1)
      end
    end
  end
end
