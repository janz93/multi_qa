# frozen_string_literal: true

require "rails_helper"

RSpec.describe Article, type: :model do
  context "validate associations" do
    it { should have_many(:translations) }
    it { should have_many(:users).dependent(:destroy) }
  end
end
