# frozen_string_literal: true

class TranslatorWorker
  include Sidekiq::Worker

  attr_reader :params

  def perform(params)
    @params = params.symbolize_keys

    Translation.create!(
      question: translated_question,
      answer: translated_answer,
      language: target_language_code,
      user: user,
      article: translation.article,
      status: "draft"
    )
  end

  private

    def user
      User.find(params[:user_id])
    end

    def translation
      @translation ||= Translation.find(params[:translation_id])
    end

    def source_language_code
      translation.language
    end

    def target_language_code
      params[:target_language]
    end

    def translator
      @translator ||= TranslatorService.new
    end

    def translated_question
      translator.translate(
        text: translation.question,
        source_language_code: source_language_code,
        target_language_code: target_language_code
      )
    end

    def translated_answer
      translator.translate(
        text: translation.answer,
        source_language_code: source_language_code,
        target_language_code: target_language_code
      )
    end
end
