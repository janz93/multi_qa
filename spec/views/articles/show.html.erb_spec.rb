# frozen_string_literal: true

require "rails_helper"

RSpec.describe "articles/show", type: :view do
  it "renders a link to translations" do
    article = create(:article)
    translation = create(
      :translation,
      question: "Eine Frage",
      answer: "Eine Antwort",
      language: "de",
      article: article
    )
    assign(:article, article)

    render

    expect(rendered).to have_link("🇩🇪", href: article_translation_path(translation.article, translation))
  end

  it "renders links to add new translation" do
    article = create(:article)
    translation = create(
      :translation,
      article: article
    )
    assign(:article, article)

    render

    expect(rendered).to have_link("New Translation", href: new_article_translation_path(article))
  end

  it "renders link back to articles" do
    article = create(:article)
    translation = create(
      :translation,
      article: article
    )
    assign(:article, article)

    render

    expect(rendered).to have_link("Back", href: articles_path)
  end
end
