# frozen_string_literal: true

require "rails_helper"

RSpec.feature "DeleteArticle", type: :feature do
  scenario "delete article" do
    article = create(:translation).article

    visit articles_path
    click_link("Destroy", href: "/articles/#{article.id}")

    expect(page).to have_content("Article was successfully destroyed.")
    expect(current_path).to eq articles_path
  end
end
