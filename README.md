# MultiQA
A multilingual Q&A tool system with automatic translation support

## Features
* User management
* Q&A article management
* Automatic translation support

## Prerequisites
This project need the following environment in order to run

- A PostgreSQL Installation at least: `postgres (PostgreSQL) 10.2`
- A Ruby Installation at least: `ruby 2.5.3p105 (2018-10-18 revision 65156) [x86_64-darwin18]`
- [An AWS Account](https://aws.amazon.com/de/account/)

## Installation
1. `git clone git@bitbucket.org:janz93/multi_qa.git`
2. `cd multi_qa`
3. `bundle install`
3. `cp .env.sample .env.development`
4. `rails db:setup`

## Testing
This is a TDD project.

To fight bugs and have best practice code we use:

* [Rspec](https://github.com/rspec/rspec)
* [Rubocop](https://github.com/bbatsov/rubocop)

Launch tests
`bundle exec rspec`

## Bugs
In case of finding bug:
Please post any bugs, questions, or ideas on our [issues page](https://bitbucket.org/janz93/multi_qa/issues).

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
