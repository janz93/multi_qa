# frozen_string_literal: true

class Article < ApplicationRecord
  has_many :translations
  has_many :users, through: :translations, dependent: :destroy

  accepts_nested_attributes_for :translations
end
