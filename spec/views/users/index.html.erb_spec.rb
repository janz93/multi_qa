# frozen_string_literal: true

require "rails_helper"

RSpec.describe "users/index", type: :view do
  it "renders link to create a new user" do
    assign(:users, [])

    render

    expect(rendered).to have_link("New User", href: new_user_path)
  end

  it "displays the user email addresses" do
    user = build_stubbed(:user, email: "test@example.com")
    assign(:users, [user])

    render

    expect(rendered).to have_content("test@example.com")
  end

  it "renders a list of users" do
    users = build_stubbed_list(:user, 2)
    assign(:users, users)

    render

    expect(rendered).to have_content("Show", count: 2)
  end

  it "renders link to profile" do
    user = build_stubbed(:user)
    assign(:users, [user])

    render

    expect(rendered).to have_link("Show", href: user_path(user))
  end

  it "renders link to edit profile" do
    user = build_stubbed(:user)
    assign(:users, [user])

    render

    expect(rendered).to have_link("Edit", href: edit_user_path(user))
  end

  it "renders link to destroy profile" do
    user = build_stubbed(:user)
    assign(:users, [user])

    render

    expect(rendered).to have_link("Destroy", href: user_path(user))
  end
end
