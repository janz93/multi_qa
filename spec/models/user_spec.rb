# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  context "validate index" do
    it { should have_db_index(:email) }
  end

  context "validate associations" do
    it { should have_many(:translations) }
    it { should have_many(:articles).dependent(:destroy) }
  end

  context "validations" do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
  end
end
