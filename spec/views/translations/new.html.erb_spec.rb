# frozen_string_literal: true

require "rails_helper"

RSpec.describe "translations/new", type: :view do
  it "renders new user form" do
    article = build_stubbed(:article)
    assign(:article, article)
    assign(:translation, Translation.new())

    render

    expect(rendered).to have_form("/articles/#{article.id}/translations", "post")
  end

  it "renders link back to article" do
    article = build_stubbed(:article)
    assign(:article, article)
    assign(:translation, Translation.new())

    render

    expect(rendered).to have_link("Back", href: article_path(article))
  end
end
