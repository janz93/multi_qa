# frozen_string_literal: true

require "rails_helper"

RSpec.feature "DeleteUser", type: :feature do
  scenario "delete user" do
    user = create(:user)

    visit users_path
    click_link("Destroy", href: "/users/#{user.id}")

    expect(page).to have_content("User was successfully destroyed.")
    expect(current_path).to eq users_path
  end
end
