# frozen_string_literal: true

require "rails_helper"

RSpec.feature "UpdateUser", type: :feature do
  scenario "change user name" do
    user = create(:user)

    visit edit_user_path(user)
    fill_in "Name", with: "James"
    click_button "Update User"

    expect(page).to have_content("James")
  end
end
