# frozen_string_literal: true

require "rails_helper"

RSpec.feature "ShowUser", type: :feature do
  scenario "view user data" do
    user = create(:user, name: "James")

    visit users_path
    click_link("Show", href: "/users/#{user.id}")

    expect(page).to have_content("James")
  end
end
