# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { "John" }
    surname  { "Doe" }
    sequence(:email) { |n| "person#{n}@example.com" }
  end
end
