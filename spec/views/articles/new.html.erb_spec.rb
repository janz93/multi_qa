# frozen_string_literal: true

require "rails_helper"

RSpec.describe "articles/new", type: :view do
  it "renders new article form" do
    assign(:article, Article.new())

    render

    assert_select "form[action=?][method=?]", articles_path, "post" do
    end
  end

  it "renders link back to articles" do
    assign(:article, Article.new())

    render

    expect(rendered).to have_link("Back", href: articles_path)
  end
end
