# frozen_string_literal: true

require "rails_helper"

RSpec.describe TranslationHelper do
  describe "#language_flag" do
    it "converts the language information into a emoji flag" do
      expect(helper.language_flag("de")).to eq("🇩🇪")
    end

    context "with the english language" do
      it "maps the english language information to the us flag" do
        expect(helper.language_flag("en")).to eq("🇺🇸")
      end
    end
    context "with the chinese languages" do
      it "maps the simplified chinese information to the chinese flag" do
        expect(helper.language_flag("zh")).to eq("🇨🇳")
      end

      it "maps the traditional chinese information to the chinese flag" do
        expect(helper.language_flag("zh-TW")).to eq("🇨🇳")
      end
    end
  end
end
