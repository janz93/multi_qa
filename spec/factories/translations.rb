# frozen_string_literal: true

FactoryBot.define do
  factory :translation do
    question { "How are you" }
    answer  { "Good" }
    language { "en" }
    user
    article
  end
end
