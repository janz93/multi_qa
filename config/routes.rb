# frozen_string_literal: true

Rails.application.routes.draw do
  root 'welcome#index'

  resources :users
  resources :articles, except: [:edit, :update] do
    resources :translations, except: :index
  end
end
