# frozen_string_literal: true

require "rails_helper"

RSpec.describe "articles/index", type: :view do
  it "renders a list of articles" do
    articles = build_stubbed_list(:article, 2)
    assign(:articles, articles)

    render

    expect(rendered).to have_content("Show", count: 2)
  end

  it "displays article id" do
    article = build_stubbed(:article, id: 42)
    assign(:articles, [article])

    render

    expect(rendered).to have_content("42")
  end

  it "renders link to article" do
    article = build_stubbed(:article)
    assign(:articles, [article])

    render

    expect(rendered).to have_link("Show", href: article_path(article))
  end

  it "renders link to destroy article" do
    article = build_stubbed(:article)
    assign(:articles, [article])

    render

    expect(rendered).to have_link("Destroy", href: article_path(article))
  end
end
