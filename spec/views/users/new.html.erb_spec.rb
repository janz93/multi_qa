# frozen_string_literal: true

require "rails_helper"

RSpec.describe "users/new", type: :view do
  it "renders new user form" do
    assign(:user, User.new())

    render

    expect(rendered).to have_form("/users", "post")
  end

  it "renders link back to profiles" do
    assign(:user, User.new())

    render

    expect(rendered).to have_link("Back", href: users_path)
  end
end
