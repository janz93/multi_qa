# frozen_string_literal: true

require "rails_helper"

RSpec.feature "CreateArticles", type: :feature do
  scenario "with valid data" do
    create(:user, name: "James")

    article_with("the answer of everything", "42", "James", "English")

    expect(page).to have_content("Article was successfully created.")
    expect(Article.count).to eq 1
    expect(Translation.count).to eq 1
  end

  scenario "with invalid data" do
    create(:user, name: "James")

    article_with("the answer of everything", "", "James", "English")

    expect(page.text).to match(/\d.error prohibited this article from being saved:/)
    expect(current_path).to eq articles_path
    expect(Article.count).to eq 0
    expect(Translation.count).to eq 0
  end

  def article_with(question, answer, user, language)
    visit new_article_path
    fill_in "Question", with: question
    fill_in "Answer", with: answer
    select(user, from: "article[translations_attributes][0][user_id]")
    select(language, from: "article[translations_attributes][0][language]")
    click_button "Create Article"
  end
end
