# frozen_string_literal: true

require "rails_helper"

RSpec.describe TranslatorService do
  describe "#translate" do
    it "returns a translation", vcr: { cassette_name: "TranslatorService/successful_translation" } do
      translator = described_class.new

      response = translator.translate(
        text: "Hello World!",
        source_language_code: "en",
        target_language_code: "de"
      )

      expect(response).to eq("Hallo Welt!")
    end
  end
end
