# frozen_string_literal: true

require "rails_helper"

RSpec.describe "translations/_form", type: :view do
  it "renders all input elements" do
    create(:user, id: 1000, name: "James")
    article = build_stubbed(:article)
    assign(:article, article)
    translation = article.translations.build
    assign(:translation, translation)

    render partial: "form", locals: { translation: translation }

    expect(rendered).to have_form("/articles/#{article.id}/translations", :post) do
      with_tag "input", with: { name: "translation[question]", type: "text" }
      with_tag "textarea", with: { name: "translation[answer]" }
      with_select("translation[user_id]", required: "required") do
        with_option("Select User")
        with_option("James", "1000")
      end
      with_select("translation[language]", required: "required") do
        with_option("Select Language")
        with_option("English", "en")
        with_option("German", "de")
      end
      with_select("translation[status]") do
        with_option("draft", "draft")
        with_option("published", "published")
      end
    end
  end
end
