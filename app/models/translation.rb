# frozen_string_literal: true

class Translation < ApplicationRecord
  belongs_to :user
  belongs_to :article

  validates :question, presence: true
  validates :answer, presence: true
  validates :language, presence: true

  after_destroy :destroy_empty_article

  enum status: %i[draft published]

  AVAILABLE_TRANSLATIONS = [
    ["Arabic", "ar"],
    ["Chinese (Simplified)", "zh"],
    ["Chinese (Traditional)", "zh-TW"],
    ["Czech", "cs"],
    ["Danish", "da"],
    ["Dutch", "nl"],
    ["English", "en"],
    ["Finnish", "fi"],
    ["French", "fr"],
    ["German", "de"],
    ["Hebrew", "he"],
    ["Indonesian", "id"],
    ["Italian", "it"],
    ["Japanese", "ja"],
    ["Korean", "ko"],
    ["Polish", "pl"],
    ["Portuguese", "pt"],
    ["Russian", "ru"],
    ["Spanish", "es"],
    ["Swedish", "sv"],
    ["Turkish", "tr"]
  ]

  def destroy_empty_article
    article.destroy if article.translations.empty?
  end
end
