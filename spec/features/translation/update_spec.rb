# frozen_string_literal: true

require "rails_helper"

RSpec.feature "UpdateTranslation", type: :feature do
  scenario "change answer" do
    translation = create(:translation, answer: "24")

    visit edit_article_translation_path(translation.article, translation)
    fill_in "Answer", with: "New answer"
    click_button "Update Translation"

    expect(page).to have_content("New answer")
  end
end
