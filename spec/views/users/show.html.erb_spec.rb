# frozen_string_literal: true

require "rails_helper"

RSpec.describe "users/show", type: :view do
  it "renders the name" do
    user = build_stubbed(
      :user,
      name: "John"
    )
    assign(:user, user)

    render

    expect(rendered).to have_content("John")
  end

  it "renders the surname" do
    user = build_stubbed(
      :user,
      surname: "Doe"
    )
    assign(:user, user)

    render

    expect(rendered).to have_content("Doe")
  end

  it "renders the email" do
    user = build_stubbed(
      :user,
      email: "john.doe@example.com"
    )
    assign(:user, user)

    render

    expect(rendered).to have_content("john.doe@example.com")
  end

  it 'renders translation question' do
    user = create(:user)
    create(:translation, question: "How are you", user: user)
    assign(:user, user)

    render
    binding.pry

    expect(rendered).to eq have_content("How are you")
  end

  it 'renders article link' do
    user = create(:user)
    article = create(:article)
    create(:translation, article: article, user: user)
    assign(:user, user)

    render

    expect(rendered).to eq have_link(article.id, href: article_path(article))
  end

  it 'renders translation link' do
    user = create(:user)
    create(:translation, question: "How are you", user: user)
    assign(:user, user)

    render

    expect(rendered).to eq have_content("How are you")
  end

  it "renders link to edit profile" do
    user = build_stubbed(:user)
    assign(:user, user)

    render

    expect(rendered).to have_link("Edit", href: edit_user_path(user))
  end

  it "renders link back to profiles" do
    user = build_stubbed(:user)
    assign(:user, user)

    render

    expect(rendered).to have_link("Back", href: users_path)
  end
end
