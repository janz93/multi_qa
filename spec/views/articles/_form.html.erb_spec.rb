# frozen_string_literal: true

require "rails_helper"

RSpec.describe "articles/_form", type: :view do
  it "renders all input elements" do
    create(:user, id: 1000, name: "James")
    article = Article.new
    article.translations.build

    render partial: "form", locals: { article: article }

    expect(rendered).to have_form("/articles", :post) do
      with_tag "input", with: { name: "article[translations_attributes][0][question]", type: "text", required: "required" }
      with_tag "textarea", with: { name: "article[translations_attributes][0][answer]", required: "required" }
      with_select("article[translations_attributes][0][user_id]", required: "required") do
        with_option("Select User")
        with_option("James", "1000")
      end
      with_select("article[translations_attributes][0][language]", required: "required") do
        with_option("Select Language")
        with_option("English", "en")
        with_option("German", "de")
      end
    end
  end
end
