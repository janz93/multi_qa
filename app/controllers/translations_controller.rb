# frozen_string_literal: true

class TranslationsController < ApplicationController
  before_action :set_article
  before_action :set_translation, except: [:new, :create]

  def show
  end

  def new
    @translation = Translation.new
  end

  def edit
  end

  def create
    @translation = @article.translations.build(translation_params)
    return create_automatic_translation if automatic_translation?
    if @translation.save
      redirect_to @article, notice: "Translation was successfully created."
    else
      render :new
    end
  end

  def update
    if @translation.update(translation_params)
      redirect_to article_translation_path(@translation.article, @translation), notice: "Translation was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @translation.destroy
    article = Article.find_by_id(params[:article_id])
    if article
      redirect_to article, notice: "Translation was successfully destroyed."
    else
      redirect_to articles_path, notice: "Translation and Article were successfully destroyed."
    end
  end


  private

    def translation_params
      params.require(:translation).permit(:id, :question, :answer, :user_id, :language, :status)
    end

    def set_translation
      @translation = Translation.find(params[:id])
    end

    def set_article
      @article = Article.find(params[:article_id])
    end

    def automatic_translation?
      params[:automatic_translation] == "1"
    end

    def create_automatic_translation
      TranslatorWorker.perform_async(
        translation_id: @article.translations.first.id,
        user_id: params[:translation][:user_id],
        target_language: params[:translation][:language]
      )
      redirect_to @article, notice: "Automatic translation will be available for review shortly."
    end
end
