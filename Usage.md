# MultiQA

## Welcome to MultiQA
MultiQA is a state of the art multilingual Q&A tool system with automatic translation support. It offers an user and Q&A article management as well as automatic translation.

## First Step
In order to use MultiQA, you need to first create an account in the user section.

## User Management
In this area, you get an overview of all users available in the system.
You can view a user profile to see what translations this person has worked on already.
Here it is also possible to update pieces of information regarding any user profile, as well as remove old user accounts.

## Article Management
Once you have a user account you can go to the article management.
Same as to the user management here you get an overview of every article available on MultiQA
You can view an article and see which in which languages this article is available. This is also the place where you add a new language version of this article to our platform.

## Translation structure
Each article must exist at least out of one language version.
Additional translation can be added manually or automatically.
If a translation was added automatically the translation needs to go through the review process. This means someone has to look at the translation and if the person is fine with the result set the status to publish. This way we ensure that each help article is understandable and correct.

Lastly here is the area to remove old articles.
