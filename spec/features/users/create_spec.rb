# frozen_string_literal: true

require "rails_helper"

RSpec.feature "user form", type: :feature do
  scenario "with valid data" do
    sign_up_with("valid@example.com", "john", "doe")

    expect(page).to have_content("User was successfully created.")
  end

  scenario "with invalid data" do
    create(:user, email: "valid@example.com")

    sign_up_with("valid@example.com", "john", "doe")

    expect(page.text).to match(/\d.error prohibited this user from being saved:/)
    expect(current_path).to eq users_path
  end

  def sign_up_with(email, name, surname)
    visit new_user_path
    fill_in "Email", with: email
    fill_in "Name", with: name
    fill_in "Surname", with: surname
    click_button "Create User"
  end
end
