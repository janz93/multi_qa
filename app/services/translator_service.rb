# frozen_string_literal: true

class TranslatorService
  def initialize
    @client = Aws::Translate::Client.new
  end

  def translate(text:, source_language_code:, target_language_code:)
    response = @client.translate_text(
      text: text,
      source_language_code: source_language_code,
      target_language_code: target_language_code
    )
    response.translated_text
  end
end
