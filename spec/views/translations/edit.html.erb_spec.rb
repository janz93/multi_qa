# frozen_string_literal: true

require "rails_helper"

RSpec.describe "translations/edit", type: :view do
  it "renders prefills the translation form" do
    translation = build_stubbed(:translation, question: "What time is it")
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_tag("input", with: { name: "translation[question]", value: "What time is it" })
  end

  it "renders link to translation" do
    translation = build_stubbed(:translation)
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_link("Show", href: article_translation_path(article, translation))
  end

  it "renders link back to article" do
    translation = build_stubbed(:translation)
    article = translation.article
    assign(:article, translation.article)
    assign(:translation, translation)

    render

    expect(rendered).to have_link("Back", href: article_path(article))
  end
end
