# frozen_string_literal: true

require "rails_helper"

RSpec.describe "users/_form", type: :view do
  it "renders all input elements" do
    user = User.new

    render partial: "form", locals: { user: user }

    expect(rendered).to have_form("/users", :post) do
      with_tag "input", with: { name: "user[email]", type: "email", placeholder: "email@example.com", required: "required" }
      with_tag "input", with: { name: "user[name]", type: "text", required: "required" }
      with_tag "input", with: { name: "user[surname]", type: "text", required: "required" }
    end
  end
end
