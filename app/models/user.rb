# frozen_string_literal: true

class User < ApplicationRecord
  has_many :translations
  has_many :articles, through: :translations, dependent: :destroy

  validates :email, presence: true, uniqueness: true
end
