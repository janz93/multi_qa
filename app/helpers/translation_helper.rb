# frozen_string_literal: true

module TranslationHelper
  def language_flag(language)
    language = language_mapping(language)
    render_flag(language)
  end

  private

    def language_mapping(language)
      return "us" if language == "en"
      return "cn" if language == "zh" || language == "zh-TW"
      language
    end

    def render_flag(language)
      ISO3166::Country[language].emoji_flag
    end
end
