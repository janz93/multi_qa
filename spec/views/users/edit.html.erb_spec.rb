# frozen_string_literal: true

require "rails_helper"

RSpec.describe "users/edit", type: :view do
  it "renders the edit user form" do
    user = build_stubbed(:user, name: "James")
    assign(:user, user)

    render

    expect(rendered).to have_tag("input", with: { name: "user[name]", value: "James" })
  end

  it "renders link to profile" do
    user = build_stubbed(:user)
    assign(:user, user)

    render

    expect(rendered).to have_link("Show", href: user_path(user))
  end

  it "renders link back to profiles" do
    user = build_stubbed(:user)
    assign(:user, user)

    render

    expect(rendered).to have_link("Back", href: users_path)
  end
end
